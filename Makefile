PLUGINS := $(patsubst %.c,%,$(wildcard *.c))

.PHONY: all clean
all: $(PLUGINS)
clean:
	rm -f $(wildcard $(PLUGINS))

%: %.c
	$(CC) -o $@ -shared -fPIC $(CFLAGS) $(LDFLAGS) $< $(LDLIBS)

DEPS :=
record: DEPS += libavcodec libavformat libavutil libswscale

$(PLUGINS): CFLAGS += $(shell pkg-config --cflags 3dsh $(DEPS))
$(PLUGINS): LDFLAGS += $(if $(DEPS),$(shell pkg-config --libs-only-L --libs-only-other $(DEPS)))
$(PLUGINS): LDLIBS += $(if $(DEPS),$(shell pkg-config --libs-only-l $(DEPS)))
