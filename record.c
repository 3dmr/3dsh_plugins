#include <limits.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/imgutils.h>
#include <libavutil/opt.h>
#include <libswscale/swscale.h>
#include <3dsh/builtins.h>
#include <3dsh/shell.h>
#include <3dsh/render.h>

struct Recorder {
    AVIOContext* ioctx;
    AVFormatContext* fctx;
    AVStream* vstrm;
    AVCodec* codec;
    AVCodecContext* cctx;
    AVFrame* frame;
    AVPacket* packet;
    char* path;
    int64_t pts;
};

static void recorder_clear(struct Recorder* recorder) {
    avio_closep(&recorder->ioctx);
    avformat_free_context(recorder->fctx);
    recorder->fctx = NULL;
    avcodec_free_context(&recorder->cctx);
    av_frame_free(&recorder->frame);
    av_packet_free(&recorder->packet);
    free(recorder->path);
    recorder->path = NULL;
}

static void recorder_destroy(struct DemoShell* shell, void* data) {
    recorder_clear(data);
    free(data);
}

static int recorder_copy(const struct Variable* src, struct Variable* dest) {
    void* data = src->data.dstruct.data;
    if (!shell_incref(data)) return 0;
    variable_make_struct(src->data.dstruct.info, data, dest);
    return 1;
}

static void recorder_free(void* data) {
    shell_decref(data);
}

static const struct FieldInfo structRecorderFields[] = {{0}};
static const struct StructInfo structRecorder = {"Recorder", structRecorderFields, recorder_copy, recorder_free};

static int variable_is_recorder(const struct Variable* v) {
    return v->type == STRUCT && v->data.dstruct.info == &structRecorder;
}

static struct Recorder* variable_to_recorder(const struct Variable* v) {
    if (variable_is_recorder(v)) {
        struct ShellRefCount* r = v->data.dstruct.data;
        struct Recorder* recorder = r->data;
        return recorder->ioctx ? recorder : NULL;
    }
    return NULL;
}

static int record_start(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct DemoShell* shell = data;
    struct Recorder* recorder;
    struct ShellRefCount* r;
    long w = -1, h = -1, fps = 60, bitrate = 400000;
    unsigned int width = shell->viewer->width, height = shell->viewer->height;
    const enum AVPixelFormat format = AV_PIX_FMT_YUV420P;

    if (!(recorder = malloc(sizeof(*recorder))) || !(r = shell_new_refcount(shell, recorder_destroy, recorder))) {
        free(recorder);
        fprintf(stderr, "Error: record_start: failed to create recorder\n");
        return 0;
    }
    recorder->ioctx = NULL;
    recorder->fctx = NULL;
    recorder->vstrm = NULL;
    recorder->codec = NULL;
    recorder->cctx = NULL;
    recorder->frame = NULL;
    recorder->packet = NULL;
    recorder->path = NULL;

    if (nArgs < 1 || !(recorder->path = variable_to_string(args))) {
        fprintf(stderr, "Error: recorder_start: expected one string path argument\n");
    } else if (nArgs == 2 || (nArgs >= 3 && (!variable_to_int(args + 1, &w) || !variable_to_int(args + 2, &h)))) {
        fprintf(stderr, "Error: recorder_start: invalid (width, height) arguments\n");
    } else if (nArgs >= 4 && !variable_to_int(args + 3, &fps)) {
        fprintf(stderr, "Error: recorder_start: invalid fps arguments\n");
    } else if (nArgs >= 5 && !variable_to_int(args + 4, &bitrate)) {
        fprintf(stderr, "Error: recorder_start: invalid bitrate arguments\n");
    } else if (avio_open(&recorder->ioctx, recorder->path, AVIO_FLAG_WRITE) < 0) {
        fprintf(stderr, "Error: recorder_start: failed to open '%s'\n", recorder->path);
    } else if (avformat_alloc_output_context2(&recorder->fctx, NULL, NULL, recorder->path) < 0) {
        fprintf(stderr, "Error: recorder_start: failed to guess format from filename\n");
    } else if (recorder->fctx->oformat->video_codec == AV_CODEC_ID_NONE) {
        fprintf(stderr, "Error: recorder_start: not a video file format\n");
    } else if (!(recorder->codec = avcodec_find_encoder(recorder->fctx->oformat->video_codec))) {
        fprintf(stderr, "Error: record_start: codec not found\n");
    } else if (!(recorder->vstrm = avformat_new_stream(recorder->fctx, NULL))) {
        fprintf(stderr, "Error: record_start: failed to allocate video strean\n");
    } else if (!(recorder->cctx = avcodec_alloc_context3(recorder->codec))) {
        fprintf(stderr, "Error: record_start: failed to allocate video codec context\n");
    } else if (!(recorder->frame = av_frame_alloc())) {
        fprintf(stderr, "Error: record_start: failed to allocate video frame\n");
    } else if (!(recorder->packet = av_packet_alloc())) {
        fprintf(stderr, "Error: record_start: failed to allocate video frame\n");
    } else {
        if (w > 0) width = (w > INT_MAX) ? ((unsigned int)INT_MAX) : ((unsigned int)w);
        if (h > 0) height = (h > INT_MAX) ? ((unsigned int)INT_MAX) : ((unsigned int)h);
        width &= ~1U;
        height &= ~1U;
        recorder->fctx->pb = recorder->ioctx;
        recorder->vstrm->id = recorder->fctx->nb_streams - 1;
        recorder->cctx->bit_rate = bitrate;
        recorder->cctx->width = width;
        recorder->cctx->height = height;
        recorder->cctx->time_base.num = 1;
        recorder->cctx->time_base.den = fps;
        recorder->cctx->gop_size = 10;
        recorder->cctx->max_b_frames = 1;
        recorder->frame->format = recorder->cctx->pix_fmt = format;
        recorder->frame->width = width;
        recorder->frame->height = height;
        recorder->pts = 0;
        if (recorder->fctx->oformat->flags & AVFMT_GLOBALHEADER) {
            recorder->cctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
        }
        if (av_frame_get_buffer(recorder->frame, 32) < 0) {
            fprintf(stderr, "Error: record_start: failed to allocate video frame data\n");
        } else if (avcodec_open2(recorder->cctx, recorder->codec, NULL) < 0) {
            fprintf(stderr, "Error: record_start: failed to open codec\n");
        } else if (avcodec_parameters_from_context(recorder->vstrm->codecpar, recorder->cctx) < 0) {
            fprintf(stderr, "Error: record_start: failed to copy stream parameters\n");
        } else if (avformat_write_header(recorder->fctx, NULL) < 0) {
            fprintf(stderr, "Error: record_start: failed to write header\n");
        } else {
            variable_make_struct(&structRecorder, r, ret);
            return 1;
        }
    }

    shell_decref(r);
    return 0;
}

static int do_frame(AVFormatContext* fctx, AVCodecContext* cctx, AVFrame* frame, AVPacket* packet, AVStream* vstrm) {
    int status, done, ok = 0;
    if (avcodec_send_frame(cctx, frame) < 0) {
        fprintf(stderr, "Error: record_frame: failed to encode frame\n");
    } else do {
        status = avcodec_receive_packet(cctx, packet);
        done = (status == AVERROR(EAGAIN) || status == AVERROR_EOF);
        ok = done || !status;
        if (!status) {
            av_packet_rescale_ts(packet, cctx->time_base, vstrm->time_base);
            packet->stream_index = vstrm->index;
            ok = av_write_frame(fctx, packet) >= 0;
            av_packet_unref(packet);
        }
    } while (ok && !done);
    return ok;
}

static int record_frame(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct DemoShell* shell = data;
    struct SwsContext* swsc;
    struct Variable tmp;
    struct Recorder* recorder;
    unsigned char *src = NULL, *swp;
    unsigned int l, nswp, srcWidth = shell->viewer->width, srcHeight = shell->viewer->height;
    enum AVPixelFormat srcFmt = AV_PIX_FMT_RGB24;
    int ptr, srcLinesize = 3 * srcWidth, ok = 0;

    if (nArgs != 1) {
        fprintf(stderr, "Error: record_frame: expected one recorder argument\n");
        return 0;
    }
    ptr = variable_move_dereference(args, &tmp);
    if (!(recorder = variable_to_recorder(&tmp))) {
        fprintf(stderr, "Error: record_frame: expected one recorder argument\n");
    } else if (!(src = malloc((srcHeight + 1) * srcLinesize))) {
        fprintf(stderr, "Error: record_frame: failed to allocate memory\n");
    } else if (pthread_mutex_lock(&shell->swapMutex)) {
        fprintf(stderr, "Error: record_frame: failed to lock shell mutex\n");
    } else {
        viewer_make_current(shell->viewer);
        if (!shell->renderDone) {
            render_frame(shell);
            shell->renderDone = 1;
            pthread_cond_signal(&shell->swapCond);
        }
        glPixelStorei(GL_PACK_ALIGNMENT, 1);
        glPixelStorei(GL_PACK_ROW_LENGTH, 0);
        glReadPixels(0, 0, srcWidth, srcHeight, GL_RGB, GL_UNSIGNED_BYTE, src);
        viewer_make_current(NULL);
        pthread_mutex_unlock(&shell->swapMutex);
        swp = src + srcHeight * srcLinesize;
        nswp = srcHeight / 2;
        for (l = 0; l < nswp; l++) {
            memcpy(swp, src + l * srcLinesize, srcLinesize);
            memcpy(src + l * srcLinesize, src + (srcHeight - 1 - l) * srcLinesize, srcLinesize);
            memcpy(src + (srcHeight - 1 - l) * srcLinesize, swp, srcLinesize);
        }

        if (!(swsc = sws_getContext(srcWidth, srcHeight, srcFmt, recorder->frame->width, recorder->frame->height, recorder->frame->format, SWS_BILINEAR, NULL, NULL, NULL))) {
            fprintf(stderr, "Error: record_frame: failed to create sws context\n");
        } else {
            sws_scale(swsc, (const unsigned char* const*)&src, &srcLinesize, 0, recorder->frame->height, recorder->frame->data, recorder->frame->linesize);
            sws_freeContext(swsc);
            recorder->frame->pts = recorder->pts;
            ok = do_frame(recorder->fctx, recorder->cctx, recorder->frame, recorder->packet, recorder->vstrm);
            recorder->pts++;
        }
    }

    if (ptr) variable_free(&tmp);
    free(src);
    ret->type = VOID;
    return ok;
}

static int record_wait(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct Variable tmp;
    struct Recorder* recorder;
    float t;
    int ptr;

    if (nArgs != 2 || !variable_to_float(args + 1, &t)) {
        fprintf(stderr, "Error: record_wait: expected a recorder and a float argument\n");
        return 0;
    }
    ptr = variable_move_dereference(args, &tmp);
    if (!(recorder = variable_to_recorder(&tmp))) {
        if (ptr) variable_free(&tmp);
        fprintf(stderr, "Error: record_wait: expected a recorder and a float argument\n");
        return 0;
    }
    recorder->pts += (t * recorder->cctx->time_base.den) / recorder->cctx->time_base.num; /* t / recorder->cctx->time_base */
    if (ptr) variable_free(&tmp);
    ret->type = VOID;
    return 1;
}

static int record_end(void* data, const struct Variable* args, unsigned int nArgs, struct Variable* ret) {
    struct Variable tmp;
    struct Recorder* recorder;
    int ok, ptr;

    if (nArgs != 1) {
        fprintf(stderr, "Error: record_end: expected one recorder argument\n");
        return 0;
    }
    ptr = variable_move_dereference(args, &tmp);
    if (!(recorder = variable_to_recorder(&tmp))) {
        if (ptr) variable_free(&tmp);
        fprintf(stderr, "Error: record_end: expected one recorder argument\n");
        return 0;
    }
    ok = do_frame(recorder->fctx, recorder->cctx, NULL, recorder->packet, recorder->vstrm)
        && !av_write_trailer(recorder->fctx);
    recorder_clear(recorder);
    ret->type = VOID;
    return ok;
}

int demo_plugin_init(struct DemoShell* shell, unsigned int apiver) {
    if (apiver != DEMO_API_VERSION) return 0;
#if LIBAVFORMAT_VERSION_INT < AV_VERSION_INT(58, 12, 100)
    av_register_all();
#endif
#if LIBAVCODEC_VERSION_INT < AV_VERSION_INT(58, 9, 100)
    avcodec_register_all();
#endif
    return builtin_function_with_data(shell, "record_start", record_start, shell)
        && builtin_function_with_data(shell, "record_frame", record_frame, shell)
        && builtin_function(shell, "record_wait", record_wait)
        && builtin_function(shell, "record_end", record_end);
}
